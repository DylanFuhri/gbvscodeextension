# goodbarber README

This is a vscode extension used by the PWA team at GoodBarber to ensure code consistency throughout the team.

---

## Features

Automatically installs required extensions.
Automatically sets default formatter and format on save.
Removes unused imports.
Pre-commit check runs linting on the files changes.

---
