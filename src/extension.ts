/* eslint-disable @typescript-eslint/no-empty-function */
/*
 * The module 'vscode' contains the VS Code extensibility API
 * Import the module and reference it with the alias vscode in your code below
 */
import * as vscode from 'vscode';

/*
 * this method is called when your extension is activated
 * your extension is activated the very first time the command is executed
 */
export function activate(context: vscode.ExtensionContext) {
    const activeEditor = vscode.window.activeTextEditor;
    const changedFiles: vscode.Uri[] = [];
    /*
     * The command has been defined in the package.json file
     * Now provide the implementation of the command with registerCommand
     * The commandId parameter must match the command field in package.json
     */
    const disposable = vscode.commands.registerCommand(
        'goodbarber.pre-commit',
        () => {
            /*
             * The code you place here will be executed every time your command is executed
             * Display a message box to the user
             */
            launchPreCommitCheck();
        }
    );

    if (activeEditor) {
        setPrettier();
        setRemoveUnusedImports();
        watchFileChange();
    }

    function launchPreCommitCheck() {
        if (!activeEditor) {
            return;
        }
        runESLint();
    }

    function setRemoveUnusedImports() {
        //const popupMessage = 'Removed Unused Imports';
        const workbenchConfig =
            vscode.workspace.getConfiguration('importSorter');
        workbenchConfig.update(
            'sortConfiguration.removeUnusedImports',
            true,
            vscode.ConfigurationTarget.Global
        );
    }

    function runESLint() {
        if (!changedFiles) {
            vscode.window.showInformationMessage('No JS/TS File Changes');
            return;
        }

        const paths: string[] = [];
        changedFiles.forEach((uri) => {
            paths.push(uri.path);
        });
        const pathString = paths.join(' ');
        const popupMessage = 'Linting...';
        const terminalCommand = `npm run lint gbpwa --files=${pathString}`;
        const terminal = vscode.window.createTerminal();
        terminal.sendText(terminalCommand);
        vscode.window.showInformationMessage(popupMessage);
    }

    function setPrettier() {
        const workbenchConfig = vscode.workspace.getConfiguration('editor');
        workbenchConfig.update(
            'defaultFormatter',
            'esbenp.prettier-vscode',
            vscode.ConfigurationTarget.Global
        );
        workbenchConfig.update('formatOnSave', 'true', true);
    }

    function watchFileChange() {
        const workspaceFolder = vscode.workspace.workspaceFolders;
        if (workspaceFolder) {
            const watcher = vscode.workspace.createFileSystemWatcher(
                new vscode.RelativePattern(workspaceFolder[0], '**/*.{ts,js}'),
                false,
                false,
                true
            );
            watcher.onDidChange((uri) => {
                if (!pathExists(uri.path)) {
                    changedFiles.push(uri);
                }
            });
        }
    }

    function pathExists(path: string): boolean {
        return changedFiles.some((el) => {
            return el.path === path;
        });
    }
}

// this method is called when your extension is deactivated
export function deactivate() {}
